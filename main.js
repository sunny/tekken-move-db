#! /usr/bin/env node
import util from 'util'

import { isMid, isMidMid, isChonky } from './filters.js'
import movelists, { queryAll } from './movelists.js'

const main = async () => {
  const chonkyMids = queryAll([isMid, isChonky])
  console.log(util.inspect(chonkyMids, true, 8))

  const lili = movelists.Lili
  const lilisMidMids = lili.query([isMidMid])
  console.log(util.inspect(lilisMidMids, true, 8))
}

main()
