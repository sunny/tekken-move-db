import * as fs from 'fs/promises'
import { parse } from 'node-html-parser'

class Movelist {
  constructor(moves) {
    this.moves = moves
  }

  get length() {
    return this.moves.length
  }

  query(filters) {
    return new Movelist(
      this.moves.filter((move) =>
        filters.every((filter) => {
          const result = filter(move)

          return result
        }),
      ),
    )
  }
}

class Hitlevel {
  constructor(text) {
    if (text.length > 1) {
      throw Error(`invalid Hitlevel: ${text}`)
    }
    this.text = text
  }

  isHigh() {
    return this.text === 'h' || this.text === 'H'
  }

  isLow() {
    return this.text === 'l' || this.text === 'L'
  }

  isMid() {
    return this.text === 'm' || this.text === 'M'
  }

  isThrow() {
    return this.text === 't'
  }

  hitsGrounded() {
    return this.text === this.text.toUppercase()
  }
}

const createMovelist = (html) => {
  const character = html
    .querySelector('.mw-page-title-main')
    .text.replace(' movelist', '')
  const moves = html
    .querySelectorAll('.movedata')
    .filter(isMove)
    .map(($movedata) => createMove($movedata, character))

  return [character, new Movelist(moves)]
}

const isMove = ($movedata) => {
  const id = $movedata.getAttribute('id')
  return Boolean(id) && id !== 'MoveDataHeader'
}

const createMove = ($movedata, character) => {
  const name = $movedata.querySelector('.movedata-name').text
  const hitLevels = $movedata
    .querySelector('.movedata-target-ctn')
    // TODO handle hitlevels like `m!` correctly
    .text.split('')
    .filter((text) => /[\w!]/.test(text))
    .map((text) => {
      // TODO regex
      const hitlevel = text.replace(/[^\w!]/g, '')
      try {
        return new Hitlevel(hitlevel)
      } catch (err) {
        throw Error(`bad hitlevel '${hitlevel}' for ${character}, move ${name}`)
      }
    })
  const damages = $movedata
    .querySelector('.movedata-damage-ctn')
    .text.replace(/[^\d,]/, '')
    .split(',')
    .map((text) => Number(text.replace(/[^\d]/, '')))
  const input = (
    $movedata.querySelector('.movedata-alt') ??
    $movedata.querySelector('.movedata-input-ctn')
  ).text.replace(/\s/g, '')

  return { name, hitLevels, damages, input }
}

const files = (await fs.readdir('./movelists')).map(
  (file) => `./movelists/${file}`,
)
const htmlstrings = await Promise.all(
  files.map((file) => fs.readFile(file, 'utf-8')),
)
const htmls = htmlstrings.map(parse)

const movelists = Object.fromEntries(htmls.map(createMovelist))
const charactersAndMovelists = Object.entries(movelists)

export const queryAll = (filters) => {
  const matching = charactersAndMovelists
    .map(([character, movelist]) => [character, movelist.query(filters)])
    .filter(([_character, movelist]) => movelist.length > 0)

  return Object.fromEntries(matching)
}

export default movelists
