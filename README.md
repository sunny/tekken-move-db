# Tekken move db

currently pretty early in development. right now it's possible to query all the movelists of the entire roster or query one character's movelist specifically. queries are just predicate functions that recieve a move. see `main.js` for a couple examples

eventually i think it'd be nice to be able to use some kind of query language to get results with just a search bar

this uses HTML files saved locally that were scraped from Wavu wiki 

## running

```bash
  npm i
  ./main.js
```
