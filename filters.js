export const isMid = (move) =>
  move.hitLevels.length === 1 && move.hitLevels[0].isMid()

export const isMidMid = (move) =>
  move.hitLevels.length === 2 &&
  move.hitLevels.every((hitLevel) => hitLevel.isMid())

export const isChonky = (move) => move.damages[0] >= 30
